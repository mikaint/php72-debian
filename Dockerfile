FROM debian:stretch

COPY ./certs/tp.pem /usr/local/share/ca-certificates/tp.crt

RUN apt-get update && \
    apt-get --assume-yes install wget gnupg dpkg curl git zip unzip && \
    apt-get --assume-yes install apt-transport-https lsb-release ca-certificates  && \
    update-ca-certificates --fresh

RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
    echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list && \
    apt-get update && \
    apt-get --assume-yes install php7.2

RUN apt-get --assume-yes install php7.2-cli \
    php7.2-common \
    php7.2-curl \
    php7.2-gd \
    php7.2-json \
    php7.2-mbstring \
    php7.0-mysql \
    php7.2-opcache \
    php7.2-readline \
    php7.2-xml \
    php7.2-tokenizer \
    php7.2-apcu \
    php7.2-bcmath \ 
    php7.2-ctype \
    php7.2-dom \
    php7.2-exif \
    php7.2-fileinfo \ 
    php7.2-gettext \
    php7.2-iconv \
    php7.2-intl \ 
    php7.2-ldap \
    php7.2-mysqli \ 
    php7.2-mysqlnd \
    #php7.2-openssl \
    #php7.2-pcntl \
    php7.2-pdo \
    #php7.2-pdo_mysql \
    #php7.2-pdo_sqlite \
    php7.2-phar \
    php7.2-posix \
    #php7.2-session \
    php7.2-shmop \
    php7.2-simplexml \
    php7.2-soap \
    php7.2-sockets \
    php7.2-sqlite3 \
    php7.2-sysvmsg \
    php7.2-sysvsem \
    php7.2-sysvshm\
    php7.2-xmlreader \
    php7.2-xmlwriter \
    php7.2-xdebug \
    php7.2-zip

#install composer     
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php --version=1.6.5 && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/local/bin/composer

#install gosu
#RUN gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4
RUN curl -o /usr/local/bin/gosu -SL "https://github.com/tianon/gosu/releases/download/1.4/gosu-$(dpkg --print-architecture)" \
    && curl -o /usr/local/bin/gosu.asc -SL "https://github.com/tianon/gosu/releases/download/1.4/gosu-$(dpkg --print-architecture).asc" \
#    && gpg --verify /usr/local/bin/gosu.asc \
    && rm /usr/local/bin/gosu.asc \
    && chmod +x /usr/local/bin/gosu

#install nodejs
RUN apt-get install --assume-yes software-properties-common && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get --assume-yes install nodejs

#install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update && \
    apt-get --assume-yes install yarn

COPY entrypoint.sh /usr/local/bin/entrypoint.sh

ENTRYPOINT ["entrypoint.sh"]

CMD ["/bin/bash"]
